<?php
// # realisation.model.php


function getDBConnexion() : PDO {
    $pdo = new PDO('mysql:host=127.0.0.1;dbname=cms2019', 'root', 'root');
    $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION); // Retourner les erreurs sql

    return $pdo;
}



function insertRealisation(string $name, string $description){
    $pdo = getDBConnexion();
    try{
        $request = $pdo->prepare("INSERT INTO `realisations` (`name`,`description`) VALUES (:name,:description)");
        $request->execute([
            'name'=> $name,
            'description'=> $description
        ]);
    }
    catch(PDOException $exception){
        die($exception->getMessage());
    }



}

function findAllRealisations(){
    $pdo = getDBConnexion();

    try{
        $request = $pdo->prepare("SELECT * FROM `realisations`");
        $request->execute();
    }
    catch(PDOException $exception){
        die($exception->getMessage());
    }

    return $request->fetchAll();

}